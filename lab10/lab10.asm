include \masm32\include\masm32rt.inc
.data
i dw 0
b dw 0
c0 dw 0
d dw 0
res dw 0
titl db "Output via function MessageBox",0; simplified window name
st1 dq 1 dup(0),0 ; message output buffer
ifmt db "%d",0ah,0ah
.code
entry_point proc
xor eax,eax ; zeroing - so as not to see garbage
mov i,3 
mov b, 2
mov c0, 4
mov ax, i
add ax, 6
mov d, ax

mov bx, i
mov cx, b
mov dx, c0

sub ax, bx
add ax, cx
sub ax, dx

mov res, ax

;movzx eax,ax ; extension (can be omitted if there is no garbage)

invoke wsprintf, ADDR st1, ADDR ifmt,res;

invoke MessageBox, 0,addr st1,addr titl, MB_OK
invoke ExitProcess, 0

entry_point endp
end