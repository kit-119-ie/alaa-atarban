;Calculate 6 values of the function Y = 25x ^ 3 - 2.1 (x varies from 1 in steps of 0.2).
NULL          	EQU 0                     	; Constants
STD_OUTPUT_HANDLE EQU -11

global Start

extern _GetStdHandle@4                      	; Import external symbols
extern _WriteFile@20                       	; Windows API functions, decorated
extern _ExitProcess@4
extern printf    	;from msvcrt
extern wsprintfA

section .bss

section .data
ifmt: db  '%d, %d, %d, %d, %d, %d,',0
_x dq 1.0
_op1 dq 25.0
_op2 dq 2.1
_zero dq 0.0
_step dq 0.2
res1 dd 0
res2 dd 0
res3 dd 0
res4 dd 0
res5 dd 0
res6 dd 0

result db 0
section .text
Start:
finit ;coprocessor initiation
lea esi, [res1]
mov ecx, 6
m1:
fld qword [_x] ; st0 -> st1; st0 = 2;
fmul qword [_x] ; st0 = x * st0 = x^2
fmul qword [_x] ; st0 = x^2 * st0 = x^3
fmul qword [_op1] ; st0 = st0 * 25
fsub qword [_op2] ; st0 = st0 - 2.1
fld qword [_x] ; st0 -> st1; st0 = x
fadd qword [_step] ; st0 = st0 + 0.2
fstp qword [_x] ; _x = st0; st1 -> st0
loop m1

 FISTP  dword [res1]; real-to-integer conversion with preservation
 FISTP  dword [res2]
 FISTP  dword [res3]
 FISTP  dword [res4]
 FISTP  dword [res5]
 FISTP  dword [res6]

mov EAX,[res1]
mov EBX,[res2]
mov ECX,[res3]
mov EDX,[res4]
mov ESI,[res5]
mov EDI,[res6]

 push    EAX
 push    EBX
 push    ECX
 push    EDX
 push    ESI
 push    EDI
 push    ifmt
 push    result 
 call    wsprintfA ;insert number into string

    	push	result
    	call	printf
    	add 	esp, 4

 push  NULL
 call  _ExitProcess@4