NULL              EQU 0                         ; Constants
STD_OUTPUT_HANDLE EQU -11


global Start

extern _ExitProcess@4
extern printf         ;from msvcrt

section .bss
name:   resb 100

section .data

struc mytype 
 
  .elem1:      resd    1 
  .elem2:      resd    1 
  .elem3:      resd    1 
  .elem4:      resd    1 
endstruc

str1:
  istruc mytype
   at mytype.elem1    
     dd 1
   at mytype.elem2
     dd 0
   at mytype.elem3 
     dd 2
   at mytype.elem4
     dd 7
  iend

str2:
  istruc mytype
   at mytype.elem1    
     dd -1
   at mytype.elem2
     dd -2
   at mytype.elem3 
     dd 5
   at mytype.elem4
     dd 0
  iend

str3:
  istruc mytype
   at mytype.elem1    
     dd -2
   at mytype.elem2
     dd -1
   at mytype.elem3 
     dd 9
   at mytype.elem4
     dd 1
  iend
  str4:
  istruc mytype
   at mytype.elem1    
     dd 3
   at mytype.elem2
     dd -3
   at mytype.elem3 
     dd -1
   at mytype.elem4
     dd 10
  iend
   str5:
  istruc mytype
   at mytype.elem1    
     dd 11
   at mytype.elem2
     dd 31
   at mytype.elem3 
     dd 5
   at mytype.elem4
     dd 5
  iend
  
titl1 db "Input matrix:",0ah," 1 -1 -2  3 11",0ah," 0 -2 -1 -3 31",0ah," 2  5  9 -1  5",0ah," 7  0  1  10 6",0ah,0,0

temp db "Min Of Col N%d:  %d",0ah,0

min db 0

need_stop db 0

section .text
Start:
push titl1
call    printf
add     esp,8
mov esi, 1
mov eax, str1
jp get_min

code1:

mov dword [need_stop], 1

mov eax, str2
jp get_min

code2:

mov dword [need_stop], 2

mov eax, str3
jp get_min

code3:

mov dword [need_stop], 3

mov eax, str4
jp get_min

code4:

mov dword [need_stop], 4

mov eax, str5
jp get_min

get_min:
mov ecx, 4
mov ebx, 0
mov edx, [min]
find_min_value:
		
cmp dword [eax], edx
jg l1

mov edx, [eax]

l1:

add eax, 4

loop find_min_value

push edx
push esi
push temp
call printf
add  esp,8

add esi, 1
cmp dword [need_stop], NULL
je code1
cmp dword [need_stop], 1
je code2
cmp dword [need_stop], 2
je code3
cmp dword [need_stop], 3
je code4
		
 push  NULL
 call  _ExitProcess@4