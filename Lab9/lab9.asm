NULL              EQU 0                         ; Constants
extern _ExitProcess@4
extern Beep
extern Sleep

global Start                                    ; Export symbols. The entry point

section .data
section .bss
section .text                                   ; Code segment
Start:

push  250
push  330
call Beep

push  250
push  294
call Beep


push  250
push  262
call Beep

push 250
push 294
call Beep
	
push  250
push  330
call Beep

push  250
push  330
call Beep

push  500
push  330
call Beep

push  250
push  330
call Beep

push  250
push  330
call Beep

push  500
push  330
call Beep


push  250
push  330
call Beep

push  250
push  392
call Beep

push  500
push  392
call Beep

push  250
push  330
call Beep
 
 push  250
push  294
call Beep

push 250
push 262
call Beep
	
push  250
push  330
call Beep

push  250
push  330
call Beep

push  250
push  330
call Beep

push  250
push  330
call Beep

push  250
push  294
call Beep

push  250
push  294
call Beep
push  250
push  330
call Beep
push  250
push  294
call Beep
push  1000
push  262
call Beep

 push  NULL
 call  _ExitProcess@4
