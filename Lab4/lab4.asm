NULL              EQU 0                     ; Constants
STD_OUTPUT_HANDLE EQU -11

extern _GetStdHandle@4              ; Import external symbols
extern _WriteFile@20                    ; Windows API functions, decorated
extern _ExitProcess@4
extern wsprintfA
extern printf
global Start                                     ; Export symbols. The entry point

section .data                                   ; Initialized data segment
 Message1        db "Result: %d" ,10, 0
 Message2        db "Input value: %d" ,10, 0
 MessageLength1  EQU $-Message1 ; Address of this line ($) - address of Message
 a dq 11
 b dq 8
 c dq 6
 array dq  11, 8, 6 
 array_count equ ($ - array) / 8   
section .bss                                    ; Uninitialized data segment
 StandardHandle resd 1
 Written        resd 1
 res resd 32
 temp resb 1
 
section .text                                    ; Code segment
Start:

mov eax, [a]
mov ebx, [b]
mov ecx, [c]


cmp ebx, eax  
JL _l1
mov ebx, eax

_l1:
cmp ebx, ecx
JG _l2
mov ebx, ecx

_l2:
 push	ebx
 push	Message1
 push	res
 call	wsprintfA                          ;insert number into string
 
 push  STD_OUTPUT_HANDLE
 call  _GetStdHandle@4
 mov   dword [StandardHandle], EAX

 push  NULL                                   ; 5th parameter
 push  Written                                 ; 4th parameter
 push  MessageLength1                 ; 3rd parameter
 push  res                                ; 2nd parameter
 push  dword [StandardHandle]      ; 1st parameter
 call  _WriteFile@20                       ; Output can be redirect to a file using >
 
    mov ecx, array_count        ; move array length to counter
	mov ebx, array	
	lp1:
	mov [temp], ecx		
	mov eax, [ebx]	              ; get current element from array
	
	push eax
	push dword Message2
	call printf                             ; and print it out
	add esp,8
		
	add ebx,8                         ; switch to next array element
		
	mov ecx, [temp]
	
	loop lp1
 
 push  NULL
 call  _ExitProcess@4
