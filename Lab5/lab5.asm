; print all array elements to the console
 NULL              EQU 0                         ; Constants

 extern _ExitProcess@4
 extern printf                                   ; from msvcrt.dll
 
 global Start                                    ; Export symbols. The entry point

 section .data                                  ; Initialized data segment
 Message1        db "Value %d",10, 0
 array dd  5, 7, 9, 11                             ; array
 array_count equ ($ - array) / 4                ; size of array

 
 section .bss
 temp resb 1
 
 section .text
 
 Start: 
 
		mov ecx, array_count        ; move array length to counter
		mov ebx, array		

		lp1:
		mov [temp], ecx		
		mov eax, [ebx]	              ; get current element from array
	
		push eax
		push dword Message1
		call printf                             ; and print it out
		add esp,8
		
		add ebx, 4                          ; switch to next array element
		
		mov ecx, [temp]
		
		loop lp1
		
	
        mov ecx, array_count        ; move array length to counter
		mov ebx, array	
		lp2:
		mov [temp], ecx		
		mov eax, [ebx + 12]	              ; get current element from array
	
		push eax
		push dword Message1
		call printf                             ; and print it out
		add esp,8
		
		sub ebx, 4                          ; switch to next array element
		
		mov ecx, [temp]
		
		loop lp2
  
 push  NULL
 call  _ExitProcess@4