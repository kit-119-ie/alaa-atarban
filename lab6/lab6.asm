NULL              EQU 0                         ; Constants
GENERIC_WRITE              EQU 0x40000000
CREATE_ALWAYS              EQU 2
FILE_ATTRIBUTE_ARCHIVE              EQU 0x80
ERROR_ALREADY_EXISTS EQU 0xb7

BSIZE1 EQU 4
BSIZE2 EQU 11
BSIZE3 EQU 7
extern wsprintfA
extern CreateFileA
extern GetLastError
extern DeleteFileA
extern CloseHandle
extern WriteFile
extern printf                                   ; from msvcrt.dll
extern _ExitProcess@4
 
global Start                                    ; Export symbols. The entry point

section .data                                   ; Initialized data segment
 fName           db "lab6.txt",0
 fmt             dd "%d ",0,0
 Buf1 dq 'test',0, 0 
 Buf12 dd 'Array: ',0, 0 
 len1 equ ($ - Buf1) / 8
 startValue dd 1
 cMessage dd 0
 fHandle dd 1
 
 Message1        db "| Reverse: ",10, 0
 array dd  5, 7, 9, 11                             ; array
 array_count equ ($ - array) / 4
 array_2 times 4 dd 0
 temp1 dd 0
 section .bss 
  temp resb 1
 section .text
 
 Start: 
 
		push NULL
		push FILE_ATTRIBUTE_ARCHIVE
		push CREATE_ALWAYS
		push NULL
		push NULL
		push GENERIC_WRITE
		push fName
		call CreateFileA
  
		mov [fHandle], eax
  
        push NULL
	    call GetLastError
		cmp eax, ERROR_ALREADY_EXISTS
		Jne l1
		
		push dword [fHandle]
		call CloseHandle
		
		push NULL
		push fName
		call DeleteFileA
		
		push NULL
		push FILE_ATTRIBUTE_ARCHIVE
		push CREATE_ALWAYS
		push NULL
		push NULL
		push GENERIC_WRITE
		push fName
		call CreateFileA
		
		mov [fHandle], eax
		
		l1:
		push NULL
		push cMessage
		push BSIZE3
		push Buf12
		push dword [fHandle]
		call WriteFile 
		mov ecx, array_count        ; move array length to counter
		mov ebx, array
		mov edx, array_2
		
	lp1:
		mov [temp], ecx	
	
		push dword [ebx]
		push fmt
		push temp1
        call wsprintfA
		
		push NULL
		push cMessage
		push BSIZE1
		push temp1
		push dword [fHandle]
		call WriteFile      
		add esp,8
		
		mov dword [edx], temp1		
		add ebx, 4                          ; switch to next array element
		add edx, 4                          ; switch to next array element
		mov ecx, [temp]
		  
	loop lp1
	
		push NULL
		push cMessage
		push BSIZE2
		push Message1
		push dword [fHandle]
		call WriteFile 
		
		mov ecx, array_count
		mov ebx, array
		mov edx, array_2
	lp2:
	
		mov [temp], ecx	
	
		push dword [ebx+ 12]
		push fmt
		push temp1
        call wsprintfA                            ; and print it out
		
		push NULL
		push cMessage
		push BSIZE1
		push temp1
		push dword [fHandle]
		call WriteFile 
		add esp,8
		
		sub ebx, 4                          ; switch to next array element
		sub edx, 4 
		mov ecx, [temp]
		
	loop lp2
		

		
		push dword [fHandle]
		call CloseHandle
  
 push  NULL
 call  _ExitProcess@4
